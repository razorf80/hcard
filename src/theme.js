const MONOXXLIGHT = '#ffffff';
const MONOXXDARK = '#000000';
const PRIMARY = '#394f63';

export const theme = {
  colorTextPrimary: `${PRIMARY}`,
  colorTextSecondary: '#bdc5c8',
  colorTextMonoXxDark: `${MONOXXDARK}`,
  colorTextMonoXxLight: `${MONOXXLIGHT}`,
  colorBorderPrimary: `${PRIMARY}`,
  colorBorderSecondary: `#cecfce`,
  colorLinePrimary: '#e2e6e8',
  colorButtonPrimary: '#758e9d',
  colorButtonHighlight: '#3fa9e2',
  colorBackgroundPrimary: '#e7e9ec',
  colorBackgroundSecondary: `${MONOXXLIGHT}`,
  colorCardHeader: `${PRIMARY}`,
  colorCardBody: `${MONOXXLIGHT}`,

  fontSizeLarge: `26px`,
  fontSizeMedium: `18px`,
  fontSizeSmall: `12px`,
  fontFamily: `Milliard, Helvetica, Arial, sansSerif`,
  fontFamilyForm: `Slabo, Helvetica, Arial, sansSerif`,

  borderRadius: `4px`,
  
  heightTextInput: `35px`,
  heightButton: `45px`,

}