import styled from 'styled-components';
import Button from '../../common/Button';
import {media} from '../../utils/device';
import H1 from '../../common/H1';
import color from 'color';

export const Wrapper = styled.div`
  height:auto;
  width: 100%; 
`

export const GridWrapper = styled.div`
  width: 100%;
  display: grid;
 
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;
  padding: 40px 40px 20px 20px;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;   

  ${media.phone`
    grid-template-columns: 1fr;
    grid-gap: 10px;
  `}
`

export const FullGrid = styled.div`
  grid-column: 1 / 3;
  ${media.phone`grid-column: 1 / 1;`}
`

export const DivTitle = styled(FullGrid)`
  border-bottom: 1px solid ${props => props.theme.colorLinePrimary};
  width: 100%;
  color: ${props => props.theme.colorTextSecondary};
  font-size: ${props => props.theme.fontSizeSmall};
  font-weight: bold;
`

export const SubmitWrapper = styled(FullGrid)`
  display: grid;
  grid-template-columns: 1fr 1fr;
  width: 100%;
  grid-gap: 20px;
`

export const ButtonForm = styled(Button)`
  font-size: ${props => props.theme.fontSizeMedium};
`

export const ButtonAvatar = styled(ButtonForm)`
  background-color: ${props => props.theme.colorButtonPrimary};
  color: ${props => props.theme.colorTextMonoXxLight};
`

export const UploadAvatar = styled.input.attrs({
  type: 'file',
  accept: 'image/*',
})`
  width: 100%;
  height: 100%;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
  cursor:pointer;
`
export const AvatarDiv = styled.div`
  position: relative;
  overflow: hidden;
  display: inline-block;
  width: 100%;
  
  :hover ${ButtonAvatar}{
    background-color: ${props => color(props.theme.colorButtonPrimary).darken(0.2).string()};
  }
}
`


export const ButtonHighlight = styled(ButtonForm)`
  background-color: ${props => props.theme.colorButtonHighlight}
  color: ${props => props.theme.colorTextMonoXxLight}

  :hover{
    background-color: ${props => color(props.theme.colorButtonHighlight).darken(0.2).string()};
  }

`
export const Title = styled(H1)`
  color: ${props => props.theme.colorTextPrimary};
  grid-column: 1 / 3;
  ${media.phone`grid-column: 1 / 1;`}
`
