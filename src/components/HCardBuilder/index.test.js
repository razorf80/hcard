/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import HCardBuilder from './index';
import sinon from 'sinon';
import {UploadAvatar, DivTitle} from './style';
import FormItem from './FormItem';


describe('<HCardBuilder/>', () => {
  const wrap = (props = {}) => shallow(<HCardBuilder {...props} />);
  
  it('values get assigned correctly', () => {
    const forms = [
      {id: 'givenName', description: 'GIVEN NAME', onChange:sinon.spy()},
      {id: 'surname', description: 'SURNAME'},
      {id: 'email', description: 'EMAIL'},
      {id: 'phone', description: 'PHONE'},
      {id: 'houseNo', description: 'HOUSE NAME OR #'},
      {id: 'street', description: 'STREET'},
      {id: 'suburb', description: 'SUBURB'},
      {id: 'state', description: 'STATE'},
      {id: 'postcode', description: 'POSTCODE'},
      {id: 'country', description: 'COUNTRY'}
    ]

    const initial = {
      onChange: sinon.spy(),
      handleAvatarChange: sinon.spy()
    }

    const wrapper = wrap(initial);

    //check form header exist
    const formHeader = wrapper.find(DivTitle);
    expect(formHeader.length).toEqual(2);
    expect(formHeader.at(0).render().text()).toEqual('PERSONAL DETAILS');
    expect(formHeader.at(1).render().text()).toEqual('ADDRESS');

    //check form items exist
    const formItems = wrapper.find(FormItem);
    expect(formItems.length).toEqual(forms.length);
    formItems.map((formItem, index) => {
      expect(formItem.prop('formId')).toEqual(forms[index].id);
      expect(formItem.prop('description')).toEqual(forms[index].description);
      expect(formItem.prop('onChange')).toBe(initial.onChange);
    })


    const avatar = wrapper.find(UploadAvatar);
    expect(avatar.length).toEqual(1);
    avatar.prop('onChange')({ target: { files: 'hello' } });
    expect(initial.handleAvatarChange.calledWith('hello')).toEqual(true);
  });

  it('not crash with no values', () => {
    const wrapper = wrap();
  });
});
