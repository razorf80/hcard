import React from 'react';
import {Wrapper, AvatarDiv} from './style'
import FormItem from './FormItem';
import PropTypes from 'prop-types';

import { 
  Title, 
  DivTitle,
  ButtonHighlight, 
  ButtonAvatar, 
  UploadAvatar,
  SubmitWrapper,
  GridWrapper
} from './style';


export const HCardBuilder = ({onChange, handleAvatarChange}) => (
  <Wrapper>
    <GridWrapper>
      <Title>hCard Builder</Title>

      <DivTitle>
        PERSONAL DETAILS
      </DivTitle>

      <FormItem
        description='GIVEN NAME'
        formId='givenName'
        onChange={onChange}
      />
      <FormItem
        description='SURNAME'
        formId='surname'
        onChange={onChange}
      />
      <FormItem
        description='EMAIL'
        formId='email'
        inputType='email'
        onChange={onChange}
      />
      <FormItem
        description='PHONE'
        formId='phone'
        inputType='tel'
        onChange={onChange}
      />

      <DivTitle>
        ADDRESS
      </DivTitle>

      <FormItem
        description='HOUSE NAME OR #'
        formId='houseNo'
        onChange={onChange}
      />
      <FormItem
        description='STREET'
        formId='street'
        onChange={onChange}
      />
      <FormItem
        description='SUBURB'
        formId='suburb'
        onChange={onChange}
      />
      <FormItem
        description='STATE'
        formId='state'
        onChange={onChange}
      />
      <FormItem
        description='POSTCODE'
        formId='postcode'
        onChange={onChange}
      />
      <FormItem
        description='COUNTRY'
        formId='country'
        onChange={onChange}
      />
      <SubmitWrapper>
        <AvatarDiv>
          <ButtonAvatar>
            Upload Avatar
          </ButtonAvatar>
          <UploadAvatar
            onChange={ (e) => handleAvatarChange( e.target.files) }/>
        </AvatarDiv>
        <ButtonHighlight>
          Create hCard
        </ButtonHighlight>
      </SubmitWrapper>
    </GridWrapper>
  </Wrapper>
)


HCardBuilder.propTypes={
  onChange:PropTypes.func,
  handleAvatarChange: PropTypes.func,
}

export default HCardBuilder