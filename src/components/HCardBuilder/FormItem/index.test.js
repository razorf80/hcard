/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import FormItem from './index';
import {Text} from './style';
import {TextInput} from '../../../common/TextInput'
import sinon from 'sinon';

describe('<FormItem/>', () => {
  const wrap = (props = {}) => shallow(<FormItem {...props} />);
  
  it('values get assigned correctly', () => {
    const initial = {
      description: 'desc',
      formId: 'formId',
      inputType: 'tel',
      onChange: sinon.spy(),
    }

    const wrapper = wrap(initial);

    const text = wrapper.find(Text);
    expect(text.length).toEqual(1);
    expect(text.render().text()).toEqual('desc');

    const textInput = wrapper.find(TextInput);
    expect(textInput.length).toEqual(1);
    expect(textInput.prop('id')).toEqual('formId');
    expect(textInput.prop('type')).toEqual('tel');

    textInput.prop('onChange')({ currentTarget: { value: 'hello' } });
    expect(initial.onChange.calledWith('formId', 'hello')).toEqual(true);

  });

  it('not crash with no values', () => {
    const wrapper = wrap();
  });
});
