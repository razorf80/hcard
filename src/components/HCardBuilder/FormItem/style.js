import styled from 'styled-components'

export const Text = styled.label`
  color: ${props => props.theme.colorTextPrimary};
  font-size: ${props => props.theme.fontSizeSmall};
`
export const Wrapper = styled.div`
  width: 100%;
`