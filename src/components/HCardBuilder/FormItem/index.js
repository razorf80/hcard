import React from 'react';
import {TextInput} from '../../../common/TextInput'
import PropTypes from 'prop-types';
import {Text, Wrapper} from './style';

const FormItem = ({description, formId, inputType, onChange}) => (
  <Wrapper>
    <Text>{description}</Text>
    <TextInput
      id={formId}
      type={inputType}
      onChange={(e) => 
        onChange(
          formId,
          e.currentTarget.value,
        )
      }
    />
  </Wrapper>
);

FormItem.propType = {
  description: PropTypes.string,
  formId: PropTypes.string,
  inputType: PropTypes.string,
  onChange: PropTypes.func,
}

FormItem.defaultProps = {
  inputType: 'text',
}

export default FormItem