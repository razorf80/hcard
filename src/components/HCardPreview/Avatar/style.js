import styled from 'styled-components';
import Person from '../../../common/icon/Person';

export const Image = styled.img`
  width: 85px;
  height: 105px;
  border: 1px solid ${props => props.theme.colorBorderSecondary};
  background-color: ${props => props.theme.colorBackgroundSecondary};
  object-fit: cover;
`

export const Wrapper = styled.div`
  width: 85px;
  height: 105px;
  background-color: ${props => props.theme.colorBackgroundSecondary};
  border: 1px solid ${props => props.theme.colorBorderSecondary};
  z-index: 1000;
  top: 60px;
  right: 10px;
  position: absolute;
`

export const AvatarIcon = styled(Person).attrs({
  fill: ({ theme }) => {return theme.colorBorderSecondary},
  width: '85px',
  height: '105px',
})`
`