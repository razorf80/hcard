import React from 'react';
import {Wrapper, Image, AvatarIcon} from './style';
import PropTypes from 'prop-types';

const Avatar = ({source}) => {
  const content = source ?
    <Image src={source}/>:
    <AvatarIcon/>;

  return(
    <Wrapper>
      {content}
    </Wrapper>
  )
}

Avatar.propTypes={
  source: PropTypes.string
}

export default Avatar