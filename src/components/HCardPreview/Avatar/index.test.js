/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import Avatar from './index';
import {Image, AvatarIcon} from './style';

describe('<Avatar/>', () => {
  const wrap = (props = {}) => shallow(<Avatar {...props} />);
  
  it('should show icon when source empty', () => {
    const wrapper = wrap();
    const icon = wrapper.find(AvatarIcon);
    expect(icon.length).toEqual(1);
  });

  it('should show image when source available', () => {
    const wrapper = wrap({source:'test'});
    const image = wrapper.find(Image);
    expect(image.length).toEqual(1);
  });
});
