import styled from 'styled-components';
import {media} from '../../utils/device';
import H2 from '../../common/H2';

export const Wrapper = styled.div`
  background-color: ${props => props.theme.colorBackgroundPrimary};
  width: 100%;
  height: 100%;
  display: grid;
  justify-items: center;
  align-items: center;
  padding: 20px;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;  

  ${media.phone`
    padding: 20px 0px 20px 0px;
  `}
`

export const RelativeWrapper = styled.div`
  position: relative;
`

export const Text = styled(H2)`
  color: ${props => props.theme.colorTextSecondary};
  text-align: right;
  margin-bottom: 5px;
`