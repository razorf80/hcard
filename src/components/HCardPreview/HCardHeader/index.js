import React from 'react';
import {Wrapper, Text} from './style';
import PropTypes from 'prop-types';

const HCardHeader = ({name}) => (
  <Wrapper>
    <Text>{name}</Text>
  </Wrapper>
)

HCardHeader.propType = {
  name: PropTypes.string,
}

export default HCardHeader