/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import HCardHeader from './index';
import {Text} from './style';

describe('<HCardHeader/>', () => {
  const wrap = (props = {}) => shallow(<HCardHeader {...props} />);

  it('should not crash on empty', () => {
    const wrapper = wrap();
  });

  it('values get assigned correctly', () => {
    const wrapper = wrap({name:"Hello World"});
    
    const text = wrapper.find(Text);
    expect(text.length).toEqual(1);
    expect(text.render().text()).toEqual('Hello World');

  });
});
