import styled from 'styled-components';

export const Wrapper = styled.div`
  height: 100px;
  width: 430px;
  background-color: ${props => props.theme.colorCardHeader};
  position: relative;
`

export const Text = styled.div`
  font-size: ${props => props.theme.fontSizeLarge};
  color: ${props => props.theme.colorTextMonoXxLight};
  bottom: 15px;
  left: 15px;
  position: absolute;
`