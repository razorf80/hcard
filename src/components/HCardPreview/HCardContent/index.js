import React from 'react';
import PropTypes from 'prop-types';
import {
  Wrapper, 
  DetailDiv,
  DescriptionDiv,
  ContentDiv} from './style';

const HCardContent = ({email, phone, address1, address2, postcode, country}) => (
  <Wrapper>
    <DetailDiv>
      <DescriptionDiv>Email</DescriptionDiv>
      <ContentDiv>{email}</ContentDiv>
    </DetailDiv>
    <DetailDiv>
      <DescriptionDiv>Phone</DescriptionDiv>
      <ContentDiv>{phone}</ContentDiv>
    </DetailDiv>
    <DetailDiv>
      <DescriptionDiv>Address</DescriptionDiv>
      <ContentDiv>{address1}</ContentDiv>
    </DetailDiv>
    <DetailDiv>
      <DescriptionDiv>&nbsp;</DescriptionDiv>
      <ContentDiv>{address2}</ContentDiv>
    </DetailDiv>
    <DetailDiv>
      <DescriptionDiv>Postcode</DescriptionDiv>
      <ContentDiv>{postcode}</ContentDiv>
      <DescriptionDiv>Country</DescriptionDiv>
      <ContentDiv>{country}</ContentDiv>
    </DetailDiv>
  </Wrapper>
)

HCardContent.propTypes = {
  email: PropTypes.string,
  phone: PropTypes.string,
  address1: PropTypes.string,
  address2: PropTypes.string,
  postcode: PropTypes.string,
  country: PropTypes.string,
}

export default HCardContent