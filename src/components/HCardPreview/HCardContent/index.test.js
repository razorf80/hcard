/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import {DetailDiv} from './style';
import HCardContent from './index';

describe('<HCardContent/>', () => {
  const wrap = (props = {}) => shallow(<HCardContent {...props} />);
  
  it('should not crash on no values', () => {
    const wrapper = wrap();
  });

  it('should  display relevant values', () => {
    const initial = {
      email: 'email_test',
      phone: 'phone_test',
      address1: 'address1_test',
      address2: 'address2_test',
      postcode: 'postcode_test',
      country: 'country_test'
    }

    const wrapper = wrap(initial);

    const items = wrapper.find(DetailDiv);
    expect(items.length).toEqual(5);

    const item1 = items.at(0).render().text();
    expect(item1).toContain('Email');
    expect(item1).toContain(initial.email);

    const item2 = items.at(1).render().text();
    expect(item2).toContain('Phone');
    expect(item2).toContain(initial.phone);

    const item3 = items.at(2).render().text();
    expect(item3).toContain('Address');
    expect(item3).toContain(initial.address1);

    const item4 = items.at(3).render().text();
    expect(item4).toContain(initial.address2);

    const item5 = items.at(4).render().text();
    expect(item5).toContain('Postcode');
    expect(item5).toContain(initial.postcode);
    expect(item5).toContain('Country');
    expect(item5).toContain(initial.country);
  });
});
