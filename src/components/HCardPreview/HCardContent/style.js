import styled from 'styled-components';

export const Wrapper = styled.div`
  height: auto;
  width: 430px;
  background-color: ${props => props.theme.colorCardBody};
  position: relative;
  padding: 15px 30px 30px 30px;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;   
`

export const DetailDiv = styled.div`
  width: 100%;
  border-bottom: 1px solid ${props => props.theme.colorBorderSecondary};
  display: flex;
  display: -ms-flexbox;
  display: -webkit-box;
  display: -webkit-flexbox;
  display: -webkit-flex;
`

export const DescriptionDiv = styled.label`
  flex: 0 0 80px;
  display: inline-block;
  color: ${props => props.theme.colorTextPrimary};
  font-size: ${props => props.theme.fontSizeSmall};
  margin-top: 15px;
  margin-bottom: 2px;
`;

export const ContentDiv = styled.label`
  width: 100%;
  display: inline;
  color: ${props => props.theme.colorTextMonoXxDark};
  font-family: ${props => props.theme.fontFamilyForm};
  margin-top: 15px;
  margin-bottom: 2px;
  margin-right: 5px;
  overflow: hidden;
  min-height: 18px;
`;