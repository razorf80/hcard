import React from  'react';
import PropTypes from 'prop-types';
import {Wrapper,RelativeWrapper,Text} from './style';
import HCardHeader from './HCardHeader';
import HCardContent from './HCardContent';
import Avatar from './Avatar';

const HCardPreview = ({name, email, phone, address1, address2, postcode, country, avatar}) => (
  <Wrapper>
    <RelativeWrapper>
      <Text>HCARD PREVIEW</Text>
      <Avatar
        source={avatar}
      />
      <HCardHeader
        name={name}
      />
      <HCardContent
        email={email}
        phone={phone}
        address1={address1}
        address2={address2}
        postcode={postcode}
        country={country}
      />
    </RelativeWrapper>
  </Wrapper>
)

HCardPreview.propTypes = {
  name: PropTypes.string,
  email: PropTypes.string,
  phone: PropTypes.string,
  address1: PropTypes.string,
  address2: PropTypes.string,
  postcode: PropTypes.string,
  country: PropTypes.string,
  avatar: PropTypes.string,
}

export default HCardPreview
