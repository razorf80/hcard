import React, {PureComponent} from 'react';
import {HCardBuilder} from '../HCardBuilder';
import { Wrapper} from './style';
import HCardPreview from '../HCardPreview';
import {joinString} from '../../utils/string';

class HCard extends PureComponent {
  constructor(props){
    super(props);
    this.state = {
      givenName: '',
      surname: '',
      email: '',
      phone: '',
      houseNo: '',
      street: '',
      suburb: '',
      state: '',
      postcode: '',
      country: '',
      avatar:'',
    }
  }

  handleFormChange = (formId, value) => {
    let object = {}
    object[formId] = value;
    this.setState(object);
  }

  handleAvatarChange = (value) => {
    if(value && window.FileReader){
        let file = value[0], reader = new FileReader(), self = this;
        reader.onload = function(r){
            self.setState({
                avatar: r.target.result
            });
        }
        try{
          reader.readAsDataURL(file);
        }catch(e){
          console.error('File Processing error ', e);
        }
    }
    else {
        console.error('Soryy, your browser does\'nt support for preview');
    }
  }

  render(){
    const name = joinString(
      [this.state.givenName, this.state.surname],
      ' '
    );
    
    const address1 = joinString(
      [this.state.houseNo, this.state.street],
      ' '
    );

    const address2 = joinString(
      [this.state.suburb, this.state.state],
      ', '
    );

    return (
      <Wrapper>
        <HCardBuilder 
          onChange={this.handleFormChange}
          handleAvatarChange={this.handleAvatarChange}/>
        <HCardPreview
          name={name}
          email={this.state.email}
          phone={this.state.phone}
          address1={address1}
          address2={address2}
          postcode={this.state.postcode}
          country={this.state.country}
          avatar={this.state.avatar}
          />
      </Wrapper>
    )
  }
}

export default HCard