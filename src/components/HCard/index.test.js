/* global describe, it, expect */
import React from 'react';
import { shallow } from 'enzyme';
import HCard from './index';
import {HCardBuilder} from '../HCardBuilder';
import HCardPreview from '../HCardPreview';
import sinon from 'sinon'

describe('<HCard/>', () => {
  const wrap = (props = {}) => shallow(<HCard {...props} />);
  
  it('checks if values get assigned correctly', () => {
    const wrapper = wrap();
    
    const builder = wrapper.find(HCardBuilder);
    expect(builder.length).toEqual(1);

    const onChangeFn = builder.prop('onChange');
    onChangeFn('givenName','hello');
    onChangeFn('surname','world');
    onChangeFn('email','hello@world.com');
    onChangeFn('phone','12345');
    onChangeFn('houseNo','12');
    onChangeFn('street','Murray St');
    onChangeFn('suburb','Pyrmont');
    onChangeFn('state','NSW');
    onChangeFn('postcode','2000');
    onChangeFn('country','Australia');
    
    const preview = wrapper.find(HCardPreview);
    expect(preview.length).toEqual(1);

    //check value get assigned correctly
    expect(preview.prop('name')).toEqual('hello world');
    expect(preview.prop('email')).toEqual('hello@world.com');
    expect(preview.prop('phone')).toEqual('12345');
    expect(preview.prop('address1')).toEqual('12 Murray St');
    expect(preview.prop('address2')).toEqual('Pyrmont, NSW');
    expect(preview.prop('postcode')).toEqual('2000');
    expect(preview.prop('country')).toEqual('Australia');

  });

  it('should call setState when handleavatar is called', () => {
    const wrapper = wrap();

    sinon.stub(FileReader.prototype, 'readAsDataURL').callsFake( function() {
      this.onload({ target: { result: 'myAvatar' }});
    });

    const builder = wrapper.find(HCardBuilder);
    expect(builder.length).toEqual(1);
    
    const avatarChange = builder.prop('handleAvatarChange');
    avatarChange('avatarSelected');

    const preview = wrapper.find(HCardPreview);
    expect(preview.prop('avatar')).toEqual('myAvatar');

  });
});
