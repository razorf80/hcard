import styled from 'styled-components';
import {media} from '../../utils/device';

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  height: 100vh; 
  justify-items: center;
  align-items: center;

  ${media.tablet`grid-template-columns: 1fr;`}
`;

export const ChildWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: grid;
`;