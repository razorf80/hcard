import styled from 'styled-components'

export const TextInput = styled.input`
  border-radius: ${props => props.theme.borderRadius};
  width: 100%;
  height: ${props => props.theme.heightTextInput};
  border: 1px solid ${props => props.theme.colorBorderSecondary};
  font-size: ${props => props.theme.fontSizeMedium};
  font-color: ${props => props.theme.colorTextMonoXxDark};
  font-family: ${props => props.theme.fontFamilyForm};
  padding-left: 8px;
  padding-right: 8px;
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;   

`