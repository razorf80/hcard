import styled from 'styled-components'

const H1 = styled.h1`
  font-size: ${props => props.theme.fontSizeLarge};
  font-weight: bold;
`

export default H1