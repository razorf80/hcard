import styled from 'styled-components'

const Button = styled.button`
  width: 100%;
  height: ${props => props.theme.heightButton};
  border-radius: ${props => props.theme.borderRadius};
  cursor:pointer;
  outline: none;
`

export default Button