import React from 'react';
import PropTypes from 'prop-types';

const Person = ({fill, width, height}) => (
  <svg version="1.1" x="0px" y="0px" viewBox="0 0 1000 1000" width={width} height={height}>
  <metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
  <g><path fill={fill} d="M990,990c0,0,0-67.4-5.6-102.6c-4.6-27.8-43.1-64.6-207-124.8c-161.3-59.2-151.3-30.4-151.3-139.3c0-70.7,36-29.6,59-163.8c8.9-52.8,16.1-17.6,35.5-102.3c10.2-44.4-6.9-47.7-4.8-68.9c2-21.2,4.1-40.1,7.9-83.5C728.2,151.1,678.4,10,500,10S271.8,151.1,276.7,205c3.8,43.1,5.9,62.3,7.9,83.5c2,21.2-15.1,24.5-4.8,68.9c19.4,84.5,26.5,49.3,35.5,102.3c23,134.2,59,93.2,59,163.8c0,109.2,10,80.4-151.3,139.3c-163.8,60-202.6,97-207,124.8C10,922.6,10,990,10,990h490H990z"/></g>
  </svg>
)

Person.propTypes = {
  fill: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
}

Person.defaultProps = {
  fill: '#000000',
  width: '100%',
  height: '100%',
}

export default Person