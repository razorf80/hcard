export const joinString = (values, join) => {
  if(values && values.length > 0){
    const result = values.filter(value => value && value.length > 0);
    if(join === undefined) join = '';
    return result.join(join);
  }else{
    return '';
  }
}