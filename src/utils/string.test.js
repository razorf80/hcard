/* global describe, it, expect */
import {joinString} from './string';

describe('joinString function', () => {
  
  it('should join both', () => {
    let result = joinString(['a','b'],'.');
    expect(result).toEqual('a.b');

    result = joinString(['a','b','c'],'/');
    expect(result).toEqual('a/b/c');
  });

  it('should not put join on empty', () => {
    let result = joinString(['','b'],'.');
    expect(result).toEqual('b');

    result = joinString(['a','','c'],'/');
    expect(result).toEqual('a/c');
  });

  it('should not crash', () => {
    let result = joinString(['','b']);
    expect(result).toEqual('b');

    result = joinString(['a','','c']);
    expect(result).toEqual('ac');

    result = joinString();
    expect(result).toEqual('');
  });
});
