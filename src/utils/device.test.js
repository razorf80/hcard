/* global describe, it, expect */
import {media} from './device';

describe('media function', () => {

  const sizesEm = {
    desktop: '80em',
    tablet: '64em',
    phone: '48em',
  } 

  it('return correct resultion', () => {

    //check phone size
    let result = (media.phone`grid-template-columns: 1fr;`).join('');
    expect(result).toContain(sizesEm.phone);
    expect(result).toContain('grid-template-columns: 1fr');

    //check tablet size
    result = (media.tablet`grid-template-columns: 1fr 1fr;`).join('');
    expect(result).toContain(sizesEm.tablet);
    expect(result).toContain('grid-template-columns: 1fr 1fr');

    //check desktop size
    result = (media.desktop`grid-template-columns: 1fr 1fr 1fr;`).join('');
    expect(result).toContain(sizesEm.desktop);
    expect(result).toContain('grid-template-columns: 1fr 1fr 1fr');
   
  });

});
